FROM python:3.7-slim-stretch

RUN apt-get update && apt-get install -y git python3-dev gcc \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt .

RUN pip install --upgrade -r requirements.txt

COPY main.py .
COPY export.pkl .

EXPOSE 5000

CMD ["/usr/local/bin/python", "main.py"]