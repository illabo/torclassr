#!/usr/local/bin/python

import sys
import string
import json
import asyncio
import aiohttp
import uvicorn
from starlette.applications import Starlette
from starlette.responses import HTMLResponse, JSONResponse
from bencoder import bdecode
from fastai.text import load_learner

app = Starlette()

# from https://github.com/render-examples/fastai-v3/blob/master/app/server.py
def load_learner_file():
    try:
        return load_learner("./")
    except RuntimeError as e:
        if len(e.args) > 0 and 'CPU-only machine' in e.args[0]:
            print(e)
            message = """
            This model was trained with an old version of fastai and will not work in a CPU environment.
            Please update the fastai library in your training environment and export your model again.
            See instructions for 'Returning to work' at https://course.fast.ai.
            """
            raise RuntimeError(message)
        else:
            raise

def prepare_metadata_string(torrent):
    info = torrent.get('info')
    if info and info.get('pieces'):
        del torrent['info']['pieces']
    return str(info).replace('.', ' ').translate(str.maketrans('', '', string.punctuation))

# based on https://stackoverflow.com/questions/33137741/fastest-way-to-convert-a-dicts-keys-values-from-bytes-to-str-in-python3
def bytes_values_to_str(data):
    if isinstance(data, bytes):
        return data.decode('utf-8', 'ignore')
    if isinstance(data, dict):
        return dict(map(bytes_values_to_str, data.items()))
    if isinstance(data, (tuple, list)):
        return list(map(bytes_values_to_str, data))
    return data

def read_file_for_prediction(file_bytes):
    tor_info = bytes_values_to_str(bdecode(file_bytes))
    return prepare_metadata_string(tor_info)

def predict(learner, info):
    prediction = learner.predict(info)
    confidence = float(max(prediction[2][0],prediction[2][1]))
    return {
        "prediction": str(prediction[0]),
        "confidence": confidence,
    }

@app.route('/check', methods=['POST'])
async def check(request):
    body = await request.body()
    tor_info = read_file_for_prediction(body)
    prediction = predict(app.state.LEARNER, tor_info)
    return JSONResponse(prediction)

def main():
    clas_learn = load_learner_file()
    app.state.LEARNER = clas_learn
    uvicorn.run(app=app, host='0.0.0.0', port=5000, log_level="info")

if __name__ == '__main__':
    main()